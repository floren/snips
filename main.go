package main

import (
	"io/ioutil"
	"syscall"
	"encoding/gob"
	"reflect"
	"bitbucket.org/floren/snips/id3"
	"bufio"
	"errors"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"time"
	"net"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
)

type Song struct {
	Name   string
	Artist string
	Album  string
	Year   string
	Track  string
	Disc   string
	Genre  string
	Length string

	File string
}

type Player struct {
	stdout *bufio.Reader
	stdin  *bufio.Writer
	cmd    *exec.Cmd
	sync.Mutex
}

var rgen *rand.Rand
var musicdirs []string
var songs map[string]Song // map filename to song
var nowplaying []Song
var player Player

var commands = map[string]func([]string) string{
	"status":   status,
	"pause":    pause,
	"ls":       ls, // list all songs
	"lsfiles":  lsfiles,
	"append":   appendsong, // append song to current playlist
	"showlist": showlist,   // show the current playlist
	"jump":     jump,       // jump to the given index in the playlist
	"next":     next,       // skip to the next song in the playlist
	"prev":     prev,
	"replace":  replace, // replace the current playlist with the given songs
	"search":   search,  // Search for matching songs
	"shuffle":	shuffle, // shuffle the current playlist
	"addall":	addall, // add every known song to the playlist
	"rescan":	rescan, // re-scan for music, either using provided directories or the defaults
}

var (
	f_mdirs    = flag.String("musicdirs", os.Getenv("HOME")+"/music", "comma-separated list of locations where music can be found")
	f_database = flag.String("database", os.Getenv("HOME")+"/.snipsdb", "location of snips database")
)

const (
	OK  = "OK\n"
	ERR = "ERR\n"
	NL  = "\n"
)

func init() {
	flag.Parse()
	rgen = rand.New(rand.NewSource(time.Now().Unix()))
	musicdirs = strings.Split(*f_mdirs, ",")
	fmt.Printf("musicdirs = %v\n", musicdirs)
	songs = make(map[string]Song)
	startmplayer()
}

func rescan(dirs []string) string {
	if len(dirs) == 0 {
		dirs = musicdirs
	}
	for _, d := range dirs {
		filepath.Walk(d, func(path string, _ os.FileInfo, _ error) error {
			s, err := id3parse(path)
			if err == nil {
				songs[s.File] = s
			}
			return nil
		})
	}
	// save off the database
	db, err := os.Create(*f_database)
	if err != nil {
		log.Print(err)
		return ERR
	}
	defer db.Close()
	enc := gob.NewEncoder(db)
	enc.Encode(songs)
	fmt.Printf("songs list = %v\n", songs)
	return OK
}

func (s Song) prettyPrint() string {
	ret := "File: " + s.File + NL
	ret += "Name: " + s.Name + NL
	ret += "Artist: " + s.Artist + NL
	ret += "Album: " + s.Album + NL
	ret += "Disc: " + s.Disc + NL
	ret += "Track: " + s.Track + NL
	ret += "Length: " + s.Length + NL
	return ret
}

func addall(args []string) string {
	list := make([]string, len(songs))
	i := 0
	for k, _ := range songs {
		list[i] = k
		i++
	}
	fmt.Println("built list")
	appendsong(list)
	return OK
}

func search(args []string) string {
	if len(args) != 2 {
		return ERR
	}

	fieldname := args[0]
	term := strings.ToLower(args[1])
	ret := ""

	// Ok, let's try reflection
	for _, s := range songs {
		v := reflect.ValueOf(s)
		if strings.Contains(strings.ToLower(v.FieldByName(fieldname).String()), term) {
			ret += s.prettyPrint()
		}
	}
	ret += OK
	return ret
}

// This is not very threadsafe!
func shuffle(args []string) string {
	if len(nowplaying) == 0 {
		return OK
	}
	order := rgen.Perm(len(nowplaying))
	//replace([]string{tmpplaying[order[0]].File})
	newsongs := make([]string, len(order))
	for i, idx := range order {
		//appendsong([]string{tmpplaying[idx].File})
		newsongs[i] = nowplaying[idx].File
	}
	//appendsong(newsongs)
	fmt.Println("time to replace")
	return replace(newsongs)
}

func status(args []string) string {
	current := currentfile()
	if c, ok := songs[current]; ok {
		return c.prettyPrint() + OK
	}
	return ERR
}

// Pause playback
func pause(args []string) string {
	player.stdin.WriteString("pause\n")
	player.stdin.Flush()
	return OK
}

// Show the current playlist
func showlist(args []string) string {
	ret := ""
	for i, s := range nowplaying {
		ret += fmt.Sprintf("Index: %d\n", i)
		ret += s.prettyPrint()
	}
	ret += OK
	return ret
}

// Replace the now-playing list with the given songs
func replace(args []string) string {
	return loadlist(args, true)
}

func loadlist(args []string, replace bool) string {
	if len(args) == 0 {
		return ERR
	}
	playlist, _ := ioutil.TempFile("/tmp", "juke")
	if replace || nowplaying == nil {
		nowplaying = []Song{}
	}
	for _, f := range args {
		if s, ok := songs[f]; ok {
			nowplaying = append(nowplaying, s)
			playlist.WriteString(f+"\n")
		}
	}
	playlist.Close()
	player.stdin.WriteString("loadlist "+playlist.Name())
	if !replace {
		player.stdin.WriteString(" 1 ")
	}
	player.stdin.WriteString("\n")
	player.stdin.Flush()
	for {
		line, err := player.stdout.ReadString('\n')
		if err != nil {
			log.Print(err)
			return ERR
		}
		if strings.HasPrefix(line, "Starting playback") {
			break
		}
	}
	os.Remove(playlist.Name())
	return OK
}

// Append the given files to the now-playing list
func appendsong(args []string) string {
	return loadlist(args, false)
}

// get the filename of the currently-playing file
func currentfile() string {
	ret := ""
	player.stdin.WriteString("get_property path\n")
	player.stdin.Flush()
	for {
		line, err := player.stdout.ReadString('\n')
		if err != nil {
			log.Print(err)
			return ""
		}
		line = strings.TrimRight(line, "\n")
		if strings.HasPrefix(line, "ANS_path=") {
			// remove prefix
			line = line[len("ANS_path="):]
			// strip any leading and trailing quotes
			line = strings.TrimRight(line, "'")
			line = strings.TrimLeft(line, "'")
			ret = line
			break
		}
	}
	return ret
}

// Jump to a specific index in the now-playing list
func jump(args []string) string {
	if len(args) == 0 {
		return ERR
	}
	idx, err := strconv.Atoi(args[0])
	if err != nil || idx > len(nowplaying) || idx < 0 {
		return ERR
	}

	// figure out what song is currently playing
	current := currentfile()

	// get the offset from that song to the one we want
	offset := 0
	for i, f := range nowplaying {
		if f.File == current {
			offset = idx - i
		}
	}
	// issue pt_step <that number>
	pt_step(offset)

	return OK
}

// Advance the playlist by the given offset
func pt_step(offset int) {
	player.stdin.WriteString(fmt.Sprintf("pt_step %d\n", offset))
	player.stdin.Flush()
}

// Next song
func next(args []string) string {
	pt_step(1)
	return OK
}

// Previous song
func prev(args []string) string {
	pt_step(-1)
	return OK
}

// Print information about all known songs (LOTS of output!)
func ls(args []string) string {
	ret := ""
	for _, s := range songs {
		ret += s.prettyPrint()
	}
	ret += OK
	return ret
}

// Print filenames for all known songs (LOTS of output!)
func lsfiles(args []string) string {
	ret := ""
	for _, s := range songs {
		ret += s.File + NL
	}
	ret += OK
	return ret
}

func handler(c net.Conn) {
	rw := bufio.NewReadWriter(bufio.NewReader(c), bufio.NewWriterSize(c, 9000000))

	rw.WriteString("OK snips\n")
	rw.Flush()
	for {
		line, err := rw.ReadString('\n')
		if err != nil {
			fmt.Println(err)
			return
		}

		line = strings.TrimRight(line, "\n")
		fmt.Println(line)

		args := strings.Split(line, " ")
		if len(args) == 0 {
			// error
			continue
		}
		if f, ok := commands[args[0]]; ok {
			arguments := []string{}
			if len(args) == 2 {
				nargs, err := strconv.Atoi(args[1])
				if err != nil {
					log.Print(err)
					return
				}
				for i := 0; i < nargs; i++ {
					tmp, err := rw.ReadString('\n')
					if err != nil {
						fmt.Println(err)
						return
					}
					tmp = strings.TrimRight(tmp, "\n")

					arguments = append(arguments, tmp)
				}
			}
			player.Lock()
			reply := f(arguments)
			player.Unlock()
			rw.WriteString(reply)
			rw.Flush()
		}
	}
}

func startmplayer() {
	os.Remove("/tmp/mplayerfifo")
	err := syscall.Mkfifo("/tmp/mplayerfifo", 600)
	if err != nil {
		log.Fatal(err)
	}
	player.cmd = exec.Command("mplayer", "-slave", "-idle", "-quiet", "file=/tmp/mplayerfifo")
	stdin, err := player.cmd.StdinPipe()
	if err != nil {
		log.Fatal(err)
	}
	stdout, err := player.cmd.StdoutPipe()
	if err != nil {
		log.Fatal(err)
	}

	player.stdin = bufio.NewWriter(stdin)
	player.stdout = bufio.NewReader(stdout)

	if err := player.cmd.Start(); err != nil {
		log.Fatal(err)
	}
}

func main() {
	if f, err := os.Open(*f_database); err == nil {
		dec := gob.NewDecoder(f)
		dec.Decode(&songs)
	} else {
		go rescan(musicdirs)
	}
	fmt.Println(songs)
	sock, err := net.Listen("unix", "/tmp/juke")
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
	defer sock.Close()

	for {
		c, err := sock.Accept()
		if err != nil {
			fmt.Println(err)
			continue
		}
		go handler(c)
	}
}

// just here so I don't forget it
func id3parse(s string) (Song, error) {
	var ret Song
	if filepath.Ext(s) != ".mp3" {
		return ret, errors.New("not an mp3 file")
	}
	ret.File = s
	var fd, err = os.Open(s)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Could not open %s: %s\n", s, err)
		return ret, err
	}
	defer fd.Close()
	file := id3.Read(fd)
	if file == nil {
		fmt.Fprintf(os.Stderr, "Could not read ID3 information from %s\n", s)
		return ret, nil // errors.New("failed to read ID3")
	} else {
		ret = Song{
			Name:   file.Name,
			Artist: file.Artist,
			Album:  file.Album,
			Year:   file.Year,
			Track:  file.Track,
			Disc:   file.Disc,
			Genre:  file.Genre,
			Length: file.Length,
			File:   s,
		}
	}
	return ret, nil
}
