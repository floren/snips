Citizen Snips!
===========

A jukebox server written in Go.

Give it a list of directories specified with the -musicdirs flag. It'll
start up, scan those directories to get info about your songs, then drop
a Unix socket in /tmp/juke.

Commands:

	status:
		get current player status
	pause:
		pause playback
	ls:
		list every song we know about
	lsfiles:
		list all files we know about
	append <files>:
		append the given file to the now playing list
	showlist:
		show the now playing list
	jump <index>:
		jump to the given index on the now playing list
	next:
		next song
	prev:
		previous song
	replace <songs>:
		replace the now playing list with the given songs
	search <field> <text>:	
		search the given field (e.g. "Artist") for the given text.

Comments that take no arguments are sent like this:

	<command name>\n

Commands that take arguments are specified like this:

	<command name> <number of arguments>\n
	<argument 1>\n
	<argument 2>\n
	...
	<argument n>\n

On successful command completion, you'll get "OK\n". If you made a
mistake, you'll get "ERR\n" (taking a leaf from ed's book here)

Protocol Examples
------------------

Search for music by Pink Floyd (case insensitive):

	search 2
	Artist
	pink floyd

TODO
-----

When you do "lsfiles", they come out in random order. We might want to
put them in order.

There's no reason why prettyPrint() can't use reflection to print out
every field.

Motivation
---------

A badger with a troubled past and nothing left to lose. An elephant
who never forgets -- to kill! And a seldom-used crab named Lucky,
a.k.a. Citizen Snips.
